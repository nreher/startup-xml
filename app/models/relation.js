import DS from 'ember-data';

export default DS.Model.extend({
    relationtype: DS.attr('string'),
    relatedtype: DS.attr('string'),
    relatedid: DS.attr('number'),
    relatedname: DS.attr('string'),
    relatedurl: DS.attr('string'),
});
