import Customizing from '../models/customizing';
import DS from 'ember-data';

export default Customizing.extend({
    states: DS.hasMany('state')
});
