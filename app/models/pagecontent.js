import DS from 'ember-data';
import Basedocumentcontent from '../models/basedocumentcontent';

export default Basedocumentcontent.extend({
    info: DS.attr('string'),
    pdf: DS.attr('string'),
    preview: DS.attr('string'),
    printpdf: DS.attr('string'),
    thumbnail: DS.attr('string'),
});
