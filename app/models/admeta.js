import DS from 'ember-data';
import Basedocumentmeta from '../models/basedocumentmeta';

export default Basedocumentmeta.extend({
    adtype: DS.attr('string'),
    customercomment: DS.attr('string'),
    customernumber: DS.attr('string'),
    ordernumber: DS.attr('string'),
    preferredlocation: DS.attr('string'),
    specialinstructions: DS.attr('string'),
});
