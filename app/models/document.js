import DS from 'ember-data';

export default DS.Model.extend({
    content: DS.belongsTo('documentcontent'),
    meta: DS.belongsTo('documentmeta'),  
});
