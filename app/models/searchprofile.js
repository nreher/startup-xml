import DS from 'ember-data';

export default DS.Model.extend({
    created: DS.attr('date'),
    createdby: DS.belongsTo('user'),
    description: DS.attr('string'),
    documenttype: DS.attr('number'),
    groups: DS.hasMany('usergroup'),
    modified: DS.attr('date'),
    modifiedby: DS.belongsTo('user'),
    name: DS.attr('string'),
    isprivate: DS.attr('boolean'),
    issystemspecific: DS.attr('boolean'),
    query: DS.attr('string'),
    revision: DS.attr('number'),
    system: DS.attr('string'),
});
