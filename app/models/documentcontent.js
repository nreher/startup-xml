import DS from 'ember-data';
import Basedocumentcontent from '../models/basedocumentcontent';

export default Basedocumentcontent.extend({
    file: DS.attr('string'),
    filesize: DS.attr('number'),
    info: DS.attr('string'),
    thumbnail: DS.attr('string'),
});
