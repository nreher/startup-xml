import DS from 'ember-data';

export default DS.Model.extend({
    bookno: DS.attr('number'),
    bookpageno: DS.attr('number'),
    createdby: DS.belongsTo('user'),
    documentvariantid: DS.attr('number'),
    edition: DS.belongsTo('edition'),
    editionid: DS.attr('number'),
    issuedate: DS.attr('date'),
    pageno: DS.attr('number'),
    pagina: DS.attr('string'),
    prodtype: DS.attr('string'),
    revision: DS.attr('number'),
    source: DS.belongsTo('sourcesystem'),
});
