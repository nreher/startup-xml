import DS from 'ember-data';

export default DS.Model.extend({
    content: DS.belongsTo('pagecontent'),
    meta: DS.belongsTo('pagemeta'),  
});
