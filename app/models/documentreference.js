import DS from 'ember-data';

export default DS.Model.extend({
    documentvariantid: DS.attr('number'),
    key: DS.attr('string'),
    sourcesystemid: DS.attr('number'),
    system: DS.belongsTo('sourcesystem'),
    url: DS.attr('string'),
});
