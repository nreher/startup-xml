import DS from 'ember-data';

export default DS.Model.extend({
    isuserwrappinggroup: DS.attr('boolean'),
    members: DS.hasMany('user'),
    name: DS.attr('string'),
});
