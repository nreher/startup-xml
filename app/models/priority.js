import Customizing from '../models/customizing';
import DS from 'ember-data';

export default Customizing.extend({
    iptc7901urgency: DS.attr('number'),
});
