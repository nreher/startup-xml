import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
    name: DS.attr('string'),
    fullname: DS.attr('string'),
    printableName: Ember.computed.any('fullname', 'name'),
    resource_uri: DS.attr('string'),
});
