import DS from 'ember-data';

export default DS.Model.extend({
    createdby: DS.belongsTo('user'),
    documentvariantid: DS.attr('number'),
    revision: DS.attr('number'),
    source: DS.belongsTo('sourcesystem'),
    tag: DS.attr('string'),
    url: DS.attr('string'),
});
