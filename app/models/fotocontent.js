import DS from 'ember-data';
import Basedocumentcontent from '../models/basedocumentcontent';

export default Basedocumentcontent.extend({
    appdata: DS.attr('string'),
    colorspace: DS.attr('number'),
    content: DS.attr('string'),
    filesize: DS.attr('number'),
    heighpx: DS.attr('number'),
    heightnm: DS.attr('number'),
    highres: DS.attr('string'),
    highresmimetype: DS.attr('string'),
    lowres: DS.attr('string'),
    is_raster_image: DS.attr('boolean'),
    plaintext: DS.attr('string'),
    portraitformat: DS.attr('boolean'),
    thumbnail: DS.attr('string'),
    widthnm: DS.attr('number'),
    widthpx: DS.attr('number'),
    xmpp: DS.attr('string'),
    exif: DS.belongsTo('exif'),
    iptc: DS.belongsTo('iptc'),
});
