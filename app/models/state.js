import Customizing from '../models/customizing';
import DS from 'ember-data';

export default Customizing.extend({
    category: DS.belongsTo('statecategory', { async: true }),
    publications: DS.hasMany('publication')
});
