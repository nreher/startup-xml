import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    display: DS.attr('string'),
    description: DS.attr('string'),
    //main: DS.attr('boolean', {defaultValue: false}),
    subElements: DS.hasMany('category', {async:true, inverse: 'parent'}),
    properties:  DS.hasMany('property', {async:true}), // sonst funktioniert hasProperties im Controller nicht
    parent: DS.belongsTo('category', {inverse: 'subElements'}),
    isExpanded: DS.attr('boolean')
});
/*.reopenClass({
 FIXTURES: [
 {id:1, name:'preferences', display:'Allgemeine Einstellungen', description:'Allgemeine Einstelllungen', subElements:
 [
 {id:1, name:'warnorphanandwidow', value:true, display:'Schusterjungen und Hurenkinder', description:'Warnung bei Schusterjungen und Hurenkindern'},
 {id:2, name:'headlinestatuscolorwidth', value:10, display:'', description:''},
 {id:3, name:'buzsuffix', value:'-BUZ', display:'', description:''}
 ]
 },
 {id:2, name:'elementshapes', display:'Pfade', description:'Vorgabe-Pfade für Elemente', subElements:
 [
 {id:1, name:'Kreis', value:'0 1.0 0.5;3 1.0 0.7761423587799072 0.7761423587799072 1.0 0.5 1.0;3 0.22385762631893158 1.0 0.0 0.7761423587799072 0.0 0.5;3 0.0 0.22385762631893158 0.22385762631893158 0.0 0.5 0.0;3 0.7761423587799072 0.0 1.0 0.22385762631893158 1.0 0.5;4'},
 {id:2, name:'Raute', value:'0 0 0.5;1 0.5 0;1 1 0.5;1 0.5 1;4'},
 {id:3, name:'Dreieck links oben', value:'0 0 0;1 1 0;1 0 1;4'},
 {id:4, name:'Dreieck rechts oben', value:'0 0 0;1 1 0;1 1 1;4'},
 {id:5, name:'Dreieck rechts unten', value:'0 1 0;1 1 1;1 0 1;4'},
 {id:6, name:'Dreieck links unten', value:'0 0 0;1 1 1;1 0 1;4'},
 ]
 },
 {id:3, name:'languages', display:'Sprachen', description:'Sprachen, die zur Auswahl angeboten werden', subElements:
 [
 {id:1, name:'Deutsch (Deutschland)', value:'de-DE', display:'Deutsch', description:''},
 {id:2, name:'Englisch (US)', value:'en-US', display:'Englisch (Vereinigte Staaten)', description:''},
 {id:3, name:'Französisch', value:'fr', display:'Französisch', description:''}
 ]
 }
 ]
 });
 */
