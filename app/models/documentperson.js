import DS from 'ember-data';

export default DS.Model.extend({
    confidence: DS.attr('number'),
    createdby: DS.belongsTo('user'),
    documentvariantid: DS.attr('number'),
    person: DS.belongsTo('person'),
    personid: DS.attr('number'),
    revision: DS.attr('number'),
    source: DS.belongsTo('sourcesystem'),
});
