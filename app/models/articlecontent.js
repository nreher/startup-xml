import DS from 'ember-data';
import Basedocumentcontent from '../models/basedocumentcontent';

export default Basedocumentcontent.extend({
    plaintext: DS.attr('string'),
    charactersws: DS.attr('number'),
    content: DS.attr('string'),
    words: DS.attr('number'),
    lines: DS.attr('number'),
    characters: DS.attr('number'),
});
