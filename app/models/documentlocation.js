import DS from 'ember-data';

export default DS.Model.extend({
    address: DS.attr('string'),
    confidence: DS.attr('number'),
    createdby: DS.belongsTo('user'),
    documentvariantid: DS.attr('number'),
    latitude: DS.attr('number'),
    longitude: DS.attr('number'),
    revision: DS.attr('number'),
    source: DS.belongsTo('sourcesystem'),
    tag: DS.attr('string'),
});
