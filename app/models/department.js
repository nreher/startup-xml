import Customizing from '../models/customizing';
import DS from 'ember-data';

export default Customizing.extend({
    parent: DS.belongsTo('department', { async: true }),
    publications: DS.hasMany('publication')
});
