import DS from 'ember-data';

export default DS.Model.extend({
    category: DS.belongsTo('category'),
    categoryid: DS.attr('number'),
    confidence: DS.attr('number'),
    createdby: DS.belongsTo('user'),
    documentvariantid: DS.attr('number'),
    revision: DS.attr('number'),
    source: DS.belongsTo('sourcesystem'),
});
