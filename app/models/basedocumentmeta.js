import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),

    created: DS.attr('date'),
    description: DS.attr('string'),
    documentcopyright: DS.attr('string'),
    documentcreator: DS.attr('string'),
    documentholderofrights: DS.attr('string'),
    documentid: DS.attr('number'),
    documentsource: DS.attr('string'),
    language: DS.attr('string'),
    mimetype: DS.attr('string'),
    modified: DS.attr('date'),
    revision: DS.attr('number'),
    validfrom: DS.attr('date'),
    validto: DS.attr('date'),
    variant: DS.attr('number'),
    variantcomment: DS.attr('string'),

    createdby: DS.belongsTo('user'),
    documentfunction: DS.belongsTo('documentfunction'),
    lock: DS.belongsTo('lock'),
    modifiedby: DS.belongsTo('user'),
    priority: DS.belongsTo('priority'),
    sourcechannel: DS.belongsTo('sourcechannel'),
    sourcesystem: DS.belongsTo('sourcesystem'),
    varianttype: DS.belongsTo('varianttype'),

    categories: DS.hasMany('documentcategory'),
    issues: DS.hasMany('documentissue'),
    keywords: DS.hasMany('documentkeyword'),
    links: DS.hasMany('documentlink'),
    locations: DS.hasMany('documentlocation'),
    persons: DS.hasMany('documentperson'),
    refs: DS.hasMany('documentreference'),

    departments: DS.hasMany('department'),
    editions: DS.hasMany('edition'),
    publications: DS.hasMany('publication'),
    states: DS.hasMany('state'),

    incomingrelations: DS.hasMany('relation'),
    outgoingrelations: DS.hasMany('relation'),
});
