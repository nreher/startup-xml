import DS from 'ember-data';
import Basedocumentmeta from '../models/basedocumentmeta';

export default Basedocumentmeta.extend({
    imagedescription: DS.attr('string'),
    imagetype: DS.attr('string'),
    specialinstructions: DS.attr('string'),
});
