import DS from 'ember-data';

export default DS.Model.extend({
    category: DS.attr('number'),
    confidence: DS.attr('number'),
    createdby: DS.belongsTo('user'),
    documentvariantid: DS.attr('number'),
    keyword: DS.belongsTo('keyword'),
    keywordid: DS.attr('number'),
    revision: DS.attr('number'),
    source: DS.belongsTo('sourcesystem'),
});
