import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    active: DS.attr('boolean', {default: false}),
    color: DS.attr('string'),
    created: DS.attr('date'),
    createdby: DS.belongsTo('user', {async: true}),
    isdefault: DS.attr('boolean', {default: false}),
    modified: DS.attr('date'),
    modifiedby: DS.belongsTo('user', { async: true }),
    revision: DS.attr('number'),
    shortname: DS.attr('string'),
    sortorder: DS.attr('number'),
    validfrom: DS.attr('date'),
    validto: DS.attr('date')
});
