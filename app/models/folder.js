import DS from 'ember-data';

export default DS.Model.extend({
    created: DS.attr('date'),
    createdby: DS.belongsTo('user'),
    description: DS.attr('string'),
    groups: DS.hasMany('usergroup'),
    isprivate: DS.attr('boolean'),
    modified: DS.attr('date'),
    modifiedby: DS.belongsTo('user'),
    name: DS.attr('string'),
    no_of_pages: DS.attr('number'),
    no_of_fotos: DS.attr('number'),
    no_of_graphics: DS.attr('number'),
    no_of_documents: DS.attr('number'),
    no_of_ads: DS.attr('number'),
    no_of_videos: DS.attr('number'),
    no_of_articles: DS.attr('number'),
    no_of_audios: DS.attr('number'),
    purposeidentifier: DS.attr('number'),
    revision: DS.attr('number'),
    topicId: DS.attr('number'),
    topicUrl: DS.attr('string'),

    incomingrelations: DS.hasMany('relation'),
    outgoingrelations: DS.hasMany('relation'),
});
