import DS from 'ember-data';

export default DS.Model.extend({
    content: DS.belongsTo('fotocontent'),
    meta: DS.belongsTo('fotometa'),  
});
