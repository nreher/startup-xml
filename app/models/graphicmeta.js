import DS from 'ember-data';
import Basedocumentmeta from '../models/basedocumentmeta';

export default Basedocumentmeta.extend({
    specialinstructions: DS.attr('string')
});
