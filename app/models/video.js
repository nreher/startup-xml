import DS from 'ember-data';

export default DS.Model.extend({
    content: DS.belongsTo('videocontent'),
    meta: DS.belongsTo('videometa'),  
});
