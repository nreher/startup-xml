import DS from 'ember-data';

export default DS.Model.extend({
    created: DS.attr('date'),
    createdby: DS.belongsTo('user'),
    documentid: DS.attr('number'),
    // lock: DS.belongsTo('lock'),
    modified: DS.attr('date'),
    modifiedby: DS.belongsTo('user'),
    revision: DS.attr('number'),
    variant: DS.attr('number'),
});
