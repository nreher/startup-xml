import DS from 'ember-data';

export default DS.Model.extend({
    alreadyheld: DS.attr('boolean'),
    locked: DS.attr('boolean'),
    lockedat: DS.attr('date'),
    lookedforcurrentsession: DS.attr('boolean'),
    username: DS.attr('string'),
});
