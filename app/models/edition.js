import Customizing from '../models/customizing';
import DS from 'ember-data';

export default Customizing.extend({
    publications: DS.hasMany('publication')
});
