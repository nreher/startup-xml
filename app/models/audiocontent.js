import DS from 'ember-data';
import Basedocumentcontent from '../models/basedocumentcontent';

export default Basedocumentcontent.extend({
    duration: DS.attr('number'),
    file: DS.attr('string'),
    filesize: DS.attr('number'),
    info: DS.attr('string'),
    thumbnail: DS.attr('string'),
    transcription: DS.attr('string'),
});
