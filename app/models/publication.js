import Customizing from '../models/customizing';
import DS from 'ember-data';

export default Customizing.extend({
    departments: DS.hasMany('department'),
    states: DS.hasMany('state')
});
