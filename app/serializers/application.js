import DS from 'ember-data';
import Ember from 'ember';

var typeIdMap = {
    1000: 'articlemeta',
    1001: 'admeta',
    1002: 'audiometa',
    1003: 'documentmeta',
    1004: 'fotometa',
    1005: 'graphicmeta',
    1006: 'pagemeta',
    1007: 'videometa',
    8101: 'topic',
};

export default DS.RESTSerializer.extend({

    normalizePayload: function(payload) {
        // remove useless information
        delete payload['customizingname'];

        // remove pre cached objects
        delete payload['category'];
        delete payload['department'];
        delete payload['documentfunction'];
        delete payload['edition'];
        delete payload['priority'];
        delete payload['publication'];
        delete payload['sourcechannel'];
        delete payload['sourcesystem'];
        delete payload['state'];
        delete payload['statecategory'];
        delete payload['varianttype'];

        // fix the crude naming of topics
        if ('cmgraphcrossmediatopic' in payload) {
            payload['topic'] = payload['cmgraphcrossmediatopic'];
            delete payload['cmgraphcrossmediatopic'];
        }

        // fix the misnamed resource documentpersons
        if ('documentpersons' in payload) {
            payload['documentperson'] = payload['documentpersons'];
            delete payload['documentpersons'];            
        }

        // process relationships
        function buildOurRelation(relation, direction) {
            var relatedid     = relation[direction];
            var relatedtype   = typeIdMap[relation[direction + 'type']];
            if (!relatedtype || !payload[relatedtype]) { return; }
            var relatedobject = payload[relatedtype].findBy('id', relatedid);
            var relatedname   = relatedobject['name'];
            var relatedurl    = relatedobject['sourceurl'];

            return {
                id:           relation.id,
                relationtype: relation._type,
                relatedtype:  relatedtype,
                relatedid:    relatedid,
                relatedname:  relatedname,
                relatedurl:   relatedurl,
            };
        }

        payload['relation'] = [];
        [
            'cmgraphassignedto', 'cmgraphtopicrelation', 'cgraphattachedto',
            'rwgraphplacedon', 'cgraphfoldercontents', 'cgraphgallerylink'
        ].forEach(function(relationType) {
            if (relationType in payload) {
                payload[relationType].forEach(function(relation) {
                    var newRelation;
                    if ('out' in relation && 'outtype' in relation) {
                        newRelation = buildOurRelation(relation, 'out');
                    } else {
                        newRelation = buildOurRelation(relation, 'in');
                    }
                    if (newRelation) {
                        payload['relation'].pushObject(newRelation);
                    }
                });
                delete payload[relationType];
            }
        });

        // process locks and sessions
        if ('lock' in payload) {
            payload.lock.forEach(function(lock) {
                delete lock.session;
                // TODO: This has to be added as soon as the session object contains some kind of id.
                // if (lock.session) {
                //     lock.username = payload.session.findBy('id', lock.session).username;
                // } else {
                //     lock.username = null;
                // }
            });
            delete payload['session'];
        }

        // cleanup the payload
        var validObjectIDs = payload._search_result[0].objects;
        Ember.$.each(typeIdMap, function(typeId, typeName) {
            if (typeName in payload) {
                var elementsToRemove = [];
                payload[typeName].forEach(function(obj) {
                    if (!validObjectIDs.contains(obj.id)) {
                        elementsToRemove.pushObject(obj);
                    }
                });
                elementsToRemove.forEach(function(obj) {
                    payload[typeName].removeObject(obj);
                });
                if (Ember.isEmpty(payload[typeName])) {
                    delete payload[typeName];
                }
            }
        });
        delete payload['_search_result'];

        return payload;
    },

    extractMeta: function(store, type, payload) {
        if (payload && payload._search_result_meta) {
            store.setMetadataFor(type, payload._search_result_meta);
            delete payload._search_result_meta;
        }
    },

    extractSingle: function(store, primaryType, payload, recordId) {
        return this._super(store, primaryType, payload, recordId);
    },

    extractArray: function(store, primaryType, payload) {
        return this._super(store, primaryType, payload);
    },
});
