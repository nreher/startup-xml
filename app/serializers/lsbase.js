import DS from 'ember-data';

export default DS.LSSerializer.extend({
    extractSingle: function(store, type, payload, id) {
        return this._super(store, type, payload, id);
    },

    extractArray: function(store, type, payload) {
        return this._super(store, type, payload);
    }
});

