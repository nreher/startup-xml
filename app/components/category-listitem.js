import Ember from 'ember';
import layout from '../templates/components/category-listitem';

export default Ember.Component.extend({
    layout: layout,

    getIndent: function() {
        var level = this.get('category').getLevel();
        var indent = '';
        for (var i = 0; i < level; i++) {
            indent += '-';
            //indent += '&nbsp;';
        }
        return indent;
    }.property('category'),

    expandedCategory: function () {
        return this.get('controller').expandedCategory;
    }.property('category'),

    actions: {
        selectCategory: function(category) {
            this.triggerAction({
                action: 'selectCategory',
                target: this.get('target'),
                actionContext: category
            });
        },
        toggleExpandCategory: function(category) {
            this.triggerAction({
                action: 'toggleExpandCategory',
                target: this.get('target'),
                actionContext: category
            });
        },
        deleteCategory: function(category) {
            this.triggerAction({
                action: 'deleteCategory',
                target: this.get('target'),
                actionContext: category
            });
        },
    }
});
