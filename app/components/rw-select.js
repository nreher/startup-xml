import Ember from 'ember';

// TODO: features:
//   - Keyboard support

var I18nProps = (Ember.I18n && Ember.I18n.TranslateableProperties) || {};

var SelectPickerComponent = Ember.Component.extend(I18nProps, {
    classNames:      ['rw-select'],
    selectAllLabel:  'Alle',
    selectNoneLabel: 'Keine',
    liveSearch:      false,
    showDropdown:    false,
    prompt:          false,
    summaryMessage:  '%@ Einträge gewählt',
    searchPlaceholder: '',
    leftIcon:'',
    rightIcon:'fa fa-caret-down',
    fixPrompt:false,

    didInsertElement: function() {
        var eventName = 'click.' + this.get('elementId');
        Ember.$(document).on(eventName, function (e) {
            if (this.get('keepDropdownOpen')) {
                this.set('keepDropdownOpen', false);
                return;
            }
            if (this.element && !Ember.$.contains(this.element, e.target)) {
                this.set('showDropdown', false);
            }
        }.bind(this));
    },

    willDestroyElement: function() {
        Ember.$(document).off('.' + this.get('elementId'));
    },

    menuButtonId: function() {
        return this.get('elementId') + '-dropdown-menu';
    }.property('elementId'),

    selectionAsArray: function() {
        var selection = this.get('selection');
        if (Ember.isArray(selection)) {
            return selection;
        } else if (Ember.isNone(selection)) {
            return [];
        } else {
            return [selection];
        }
    },

    contentPathName: function(pathName) {
        return this.getWithDefault(pathName, '').substr(8);
    },

    getByContentPath: function(obj, pathName) {
        return Ember.get(obj, this.contentPathName(pathName));
    },

    contentList: function() {
        var lastGroup;
        // Ember.Select does not include the content prefix for optionGroupPath
        var groupPath = this.get('optionGroupPath');
        // Ember.Select expects optionLabelPath and optionValuePath to have a
        // `content.` prefix
        var labelPath = this.get('optionLabelPath');
        var valuePath = this.get('optionValuePath');
        // selection is either an object or an array of object depending on the
        // value of the multiple property. Ember.Select maintains the value
        // property.
        var selection     = this.selectionAsArray();
        var searchMatcher = this.makeSearchMatcher();


        var result = this.get('content')
            .map(function(item) {
                var label = Ember.get(item, labelPath.replace('content.',''));
                var value = Ember.get(item, valuePath.replace('content.',''));
                var group = groupPath ? Ember.get(item, groupPath) : null;
                if (group === lastGroup) {
                    group = null;
                } else {
                    lastGroup = group;
                }
                return {
                    item:     item,
                    group:    group,
                    label:    label,
                    value:    value,
                    selected: selection.contains(item)
                };
            })
            .filter(function (item) {
                return (searchMatcher(item.group) || searchMatcher(item.label));
            });

        if (result[0]) {
            result[0].first = true;
        }

        return result;
    }.property('selection.@each', 'content.@each', 'optionGroupPath', 'optionLabelPath', 'optionValuePath', "searchFilter"),

    selectedContentList: Ember.computed.filterBy('contentList', 'selected'),
    unselectedContentList: Ember.computed.setDiff('contentList', 'selectedContentList'),

    makeSearchMatcher: function () {
        var filter = this.get('searchFilter');

        if (filter === null || filter === undefined) {
            return function () {
                return true;
            };
        } else {
            return function (item) {
                return item && item.toLowerCase().indexOf(filter.toLowerCase()) >= 0;
            };
        }
    },

    selectionSummary: function() {

        // Sofern fixPrompt gesetzt wird, ändert sich der Text der Combo nicht, selbst wenn
        // mehrfach selektiert wird.

        if (this.get('fixPrompt')) {
            return this.get('prompt');
        } else {
            var selection = this.selectionAsArray();
            switch (selection.length) {
                // I18n done by promptTranslate property (I18n plugin)
                case 0:
                    return this.get('prompt') || 'Nothing Selected';
                case 1:
                    return this.getByContentPath(selection[0], 'optionLabelPath');
                default:
                    if (Ember.I18n) {
                        return Ember.I18n.t(this.get('summaryMessage'), {count: selection.length});
                    } else {
                        return this.get('summaryMessage').fmt(selection.length);
                    }
            }
        }
    }.property('selection.@each'),

    toggleSelection: function(value) {
        var selection = this.get('selection');
        if (selection.contains(value)) {
            selection.removeObject(value);
        } else {
            selection.pushObject(value);
        }
    },

    actions: {
        selectItem: function(selected) {
            this.set('keepDropdownOpen', true);
            if (!this.get('disabled')) {
                if (this.get('multiple')) {
                    this.toggleSelection(selected.item);
                } else {
                    this.set('selection', selected.item);
                    this.toggleProperty('showDropdown');
                }
            }

            return true;
        },

        showHide: function () {
            this.toggleProperty('showDropdown');
        },

        selectAllNone: function (listName) {
            this.get(listName)
                .forEach(function (item) {
                    this.send('selectItem', item);
                }.bind(this));
        }
    }

});

export default SelectPickerComponent;
