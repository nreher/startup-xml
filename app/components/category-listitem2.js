import Ember from 'ember';
import layout from '../templates/components/category-listitem2';

export default Ember.Component.extend({
  layout: layout,

    actions: {
        selectCategory: function(category) {
            this.triggerAction({
                action: 'selectCategory',
                target: this.get('target'),
                actionContext: category
            });
        },
        toggleExpandCategory: function(category) {
            this.triggerAction({
                action: 'toggleExpandCategory',
                target: this.get('target'),
                actionContext: category
            });
        },
        deleteCategory: function(category) {
            this.triggerAction({
                action: 'deleteCategory',
                target: this.get('target'),
                actionContext: category
            });
        },
    }
});
