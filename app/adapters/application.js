import Ember from 'ember';
import DS from 'ember-data';
import ENV from '../config/environment';

var ApplicationAdapter = DS.RESTAdapter.extend({

    host: function () {
        return ENV.APP.ATLAS_API_URL;
    }.property(),

    ajax: function (url, type, hash) {
        // init the hash structure so we can fill it later on
        if (Ember.isEmpty(hash)) { hash = {}; }
        if (Ember.isEmpty(hash.data)) { hash.data = {}; }
        // tell jquery that this is a crossdomain request and that we always sent credentials
        hash.crossDomain = true;
        hash.xhrFields = { withCredentials: true };
        // switch the red.web database serializer to json-flat mode
        hash.data.format = 'json-flat';
        // do it, baby!
        return this._super(url, type, hash);
    },

    pathForType: function (type) {
        switch (type) {
            case 'folder':
            case 'user':
            case 'searchprofile':
            case 'sourcesystem':
            case 'usergroup':
                return type;
            case 'category':
            case 'edition':
            case 'department':
            case 'publication':
            case 'state':
            case 'statecategory':
            case 'priority':
                return 'customizing/' + type;
            case 'topic':
                return 'graph/node/cmgraphcrossmediatopic';
            case 'documentperson':
                return 'documentpersons';
            default:
                return 'documents/' + type;
        }
    },

    findQuery: function (store, type, query) {
        return this._super(store, type, {
            query: JSON.stringify(query),
        });
    }
});


export default ApplicationAdapter;
