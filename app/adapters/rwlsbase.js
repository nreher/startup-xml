import DS from 'ember-data';

export default DS.LSAdapter.extend({
    updateRecord: function (store, type, record) {
        return this._super(store,type,record);
    }
});
