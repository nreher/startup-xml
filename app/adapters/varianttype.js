import RWLSBase from '../adapters/rwlsbase';

export default RWLSBase.extend({
    namespace: 'app.varianttype'
});
