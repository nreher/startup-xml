import Ember from 'ember';

export function indent(params) {
    return '&bull;&ensp;&ensp;';
}

export default Ember.HTMLBars.makeBoundHelper(indent);
