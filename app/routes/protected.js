import Ember from 'ember';

export default Ember.Route.extend({
    // validate authentication.
    beforeModel: function (transition) {
        if (!this.session.get('isAuthenticated')) {
            this.controllerFor('login').set('attemptedTransition', transition);
            this.transitionTo('login');
        }
        this.controllerFor('application').set('showNavbar', true);
    },

    // common actions for all protected routes
    actions: {
        cancelForm: function () {
            history.back();
        }
    }
});

