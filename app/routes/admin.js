import Ember from 'ember';
import ProtectedRoute from '../routes/protected';

export default ProtectedRoute.extend({
    model: function () {
        return Ember.RSVP.hash({
            publications: this.store.find('publication'),
            departments: this.store.find('department'),
            priorities: this.store.find('priority'),
            editions: this.store.find('edition'),
            statecategories: this.store.find('statecategory'),
            states: this.store.find('state')
        });
    }
});
