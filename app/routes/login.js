import ENV from '../config/environment';
import Ember from 'ember';

export default Ember.Route.extend({
    beforeModel: function () {
        this.controllerFor('application').set('showNavbar', false);
    },

    actions: {
        login: function () {
            var self = this;

            return Ember.$.post(ENV.APP.ATLAS_API_URL + '/session',
                JSON.stringify({
                    username: self.controller.get('username') || 'dossm',
                    password: self.controller.get('password') || 'mdossi'
                })
            ).then(
                // successful login
                function (data) {
                    self.controller.set('username', '');
                    self.controller.set('password', '');
                    self.session.setProperties(data);

                    self.store.find('user', {
                        select: ['id'],
                        where: {field: {name: 'name', comparator: '=', value: data.username}}
                    }).then(function (response) {
                        var user = response.get('firstObject');
                        self.session.set('userid', user.get('id'));
                        self.session.set('fullname', user.get('fullname'));
                        Ember.$.getJSON(user.get('resource_uri')).then(function(d) {
                            if (!Ember.isEmpty(d.profile) && !Ember.isEmpty(d.profile.avatarimage)) {
                                self.session.set('avatarimage', d.profile.avatarimage);
                            }
                        });
                    });


                    var attemptedTransition = self.controller.get('attemptedTransition');
                    if (attemptedTransition) {
                        attemptedTransition.retry();
                        self.controller.set('attemptedTransition', null);
                    } else {
                        self.transitionTo('dashboard');
                    }
                },
                // failed login
                function (jqXHR) {
                    var errorMessage = jqXHR.responseText;
                    if (jqXHR.responseJSON && jqXHR.responseJSON.error) {
                        errorMessage = jqXHR.responseJSON.error;
                    }
                    self.controller.set('errorMessage', errorMessage);
                }
            );
        }
    }
});
