import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
    location: config.locationType
});

Router.map(function () {
  this.route("dashboard", { path: '/' });
  this.route("login");
  this.route("settings");
  this.route("admin");
  this.route("showcomponents");
  this.route("categories", { path: '/redweb-Kategorien'}, function () {
    this.route("category", { path: '/:display' });
  });
});

export default Router;
