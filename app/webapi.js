import Ember from 'ember';

function WebAPI(restURL, username, ssoKey, password) {
    this.restURL = restURL;
    this.username = username;
    this.password = password;
    this.ssoKey = ssoKey;
    this.sessionID = null;
}

WebAPI.prototype.callError = function () {
    alert('Beim Verbinden mit der WebAPI ist ein Fehler aufgetreten');
};

WebAPI.prototype.call = function (methodSpec, callBack) {
    var data = {};
    for (var i = 0; i < arguments.length - 2; i++) {
        data['arg' + i] = JSON.stringify(arguments[i + 2]);
    }

    if (this.username) {
        data['dv_user'] = this.username;
    }

    if (this.sessionID) {
        data['dv_password'] = this.sessionID;
    }

    Ember.$.ajax({
        dataType: "text",
        dataFilter: function (data) {
            if (data.length === 0) {
                return undefined;
            }
            return Ember.$.parseJSON(data);
        },
        url: this.restURL + methodSpec,
        data: data,
        success: callBack,
        error: this.callError,
        headers: {"Authorization": null}
    });
};

WebAPI.prototype.doWithSession = function (handler) {
    var pp = this;
    this.call(
        'SessionService/loginForApplication',
        function (data) {
            if (data[1] == null || data[1] === ':null:') {
                alert('Login im Print Publisher gescheitert: ' + data[3]);
                return;
            }
            pp.sessionID = data[1];
            handler();
        },
        this.username,
        this.ssoKey,
        this.password,
        null,
        'Atlas Asset Manager'
    );
};

WebAPI.prototype.closeSession = function () {
    this.call(
        'SessionService/logout',
        function () {},
        this.username,
        this.sessionID
    );
};

export default WebAPI;
