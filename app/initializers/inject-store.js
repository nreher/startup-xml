export default {
    name: 'inject-store',
    after: 'store',

    initialize: function (container) {
        container.injection('component', 'store', 'store:main');
    }
};
