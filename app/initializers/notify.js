import Ember from 'ember';

var Notify = Ember.Object.extend({
    send: function(title, body) {
        var icon;
        var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        var isChrome = !!window.chrome && !isOpera;              // Chrome 1+

        if (isChrome || isOpera) {
            icon = '/assets/images/notificationicon.png';
        }

        /* global Notification */

        if (!('Notification' in window)) {
            return;
        } else if (Notification.permission === 'denied') {
            return;
        } else if (Notification.permission === 'granted') {
            new Notification(title, { body: body, icon: icon });
        } else {
            Notification.requestPermission(function (permission) {
                if (permission === 'granted') {
                    new Notification(title, { body: body, icon: icon });
                }
            });
        }
    },
});

export function initialize(container) {
    container.register('notify:current', Notify, { singleton: true });
    container.injection('controller', 'notification', 'notify:current');
    container.injection('route', 'notification', 'notify:current');
    container.injection('component', 'notification', 'notify:current');
}

export default {
    name: 'notify',
    initialize: initialize
};
