import Ember from 'ember';
import ENV from '../config/environment';

export function initialize(container) {
    container.register('session:current', Session, {singleton: true});
    container.injection('controller', 'session', 'session:current');
    container.injection('route', 'session', 'session:current');
    container.injection('component', 'session', 'session:current');

    // pump the store into the session object
    var store = container.lookup('store:main');
    var session = container.lookup('session:current');
    session.set('store', store);
}

export default {
    name: 'session',
    after: 'store',
    initialize: initialize
};

var Session = Ember.Object.extend({
    init: function () {
        this._super();

        this.set('ssokey', localStorage.ssokey);
        this.set('sessionkey', localStorage.sessionkey);
        this.set('userid', localStorage.userid);
        this.set('username', localStorage.username);
        this.set('fullname', localStorage.fullname);
        this.set('avatarimage', localStorage.avatarimage);

        // install the ajax handler that adds basic auth information
        var self = this;
        Ember.$.ajaxPrefilter(function (options, originalOptions, xhr) {
            if (!Ember.isEmpty(self.get('username')) && !Ember.isEmpty(self.get('sessionkey'))) {
                xhr.setRequestHeader(
                    'Authorization',
                    'Basic ' + btoa(self.get('username') + ':' + self.get('sessionkey')));
            }
        });
    },

    isAuthenticated: function () {
        return (!Ember.isEmpty(this.get('username')) &&
                !Ember.isEmpty(this.get('ssokey')) &&
                !Ember.isEmpty(this.get('sessionkey')));
    }.property('username', 'ssokey', 'sessionkey'),

    reset: function () {
        this.set('ssokey', '');
        this.set('sessionkey', '');
        this.set('userid');
        this.set('username', '');
        this.set('fullname', '');
        this.set('avatarimage', '');
    },

    syncSessionData: function() {
        if (Ember.isEmpty(this.get('sessionkey'))) {
            localStorage.clear();
        } else {
            // sync session to localStorage
            localStorage.ssokey = this.get('ssokey');
            localStorage.sessionkey = this.get('sessionkey');
            localStorage.userid = this.get('userid');
            localStorage.username = this.get('username');
            localStorage.fullname = this.get('fullname');
            localStorage.avatarimage = this.get('avatarimage');
        }
    }.observes('ssokey', 'sessionkey', 'userid', 'username', 'fullname', 'avatarimage'),

    syncToCustomizationData: function () {
        if (!Ember.isEmpty(this.get('sessionkey'))) {
            // fill localStorage with customization data
            var store = this.get('store');
            var host = ENV.APP.ATLAS_API_URL.replace(/\/$/, '') + '/';

            // Download customizing data
            [
                'sourcesystem',
                'sourcechannel',
                'customizing/category',
                'customizing/department',
                'customizing/documentfunction',
                'customizing/edition',
                'customizing/priority',
                'customizing/publication',
                'customizing/statecategory',
                'customizing/state',
                'customizing/varianttype'
            ].forEach(function (datatype) {
                    var resultkey = datatype.split('/').slice(-1)[0];
                    Ember.$.ajax({
                        url: host + datatype,
                        dataType: 'json',
                        async: false,
                        data: {format: 'json-flat'},
                        success: function (result) {
                            if (!Ember.isEmpty(result[resultkey])) {
                                store.pushMany(resultkey, result[resultkey]).forEach(function(item) {
                                    item.save();
                                });
                            }
                        }
                    });
                });
        }
    }.observes('store', 'sessionkey')
});
