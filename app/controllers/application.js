import Ember from 'ember';

export default Ember.Controller.extend({
    breadCrumb: 'Start',
    actions: {
        logout: function () {
            this.session.reset();
            this.transitionToRoute('login');
        }
    }
});
