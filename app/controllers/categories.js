import Ember from 'ember';

export default Ember.ArrayController.extend({
    breadCrumb: 'redweb.xml',

    createMainCategoryChecked: true,

    getCategories: function() {
      return this.get('model');
    }.property('model'),

    hasNoSelectedCategory: function () {
        return this.get('selectedCategory') === undefined;
    }.property('selectedCategory'),

    isNotEditingNewCategory: function() {
        return Ember.isEmpty(this.get('newCategoryName'));
    }.property('newCategoryName'),

    actions: {

        toggleExpandCategory: function(category) {
          category.set('isExpanded', !category.get('isExpanded'));
        },

        createCategory: function() {
            var self = this;
            var newCategory = self.get("newCategoryName");
            if (!Ember.isEmpty(newCategory)) {
                var parentCategory;
                if (!self.createMainCategoryChecked &&
                    !Ember.isEmpty(self.get('selectedCategory'))) {
                    parentCategory = self.get('selectedCategory');
                    console.log("parentCategory: " + parentCategory);
                }
                // evtl. parent aufklappen
                if (!Ember.isEmpty(parentCategory)) {
                    parentCategory.set('isExpanded', true);
                }
                var category = this.store.createRecord('category', {
                    name: newCategory,
                    display: newCategory,
                    description: 'keine Beschreibung',
                    main: true,
                    parent: parentCategory
                });
                category.save();
                if (!Ember.isEmpty(parentCategory)) {
                    parentCategory.get('subElements').pushObject(category);
                }
                else {
                    self.get('model').pushObject(category);
                }
                console.log("new Catagory: " + category);
                self.transitionToRoute('categories.category', category);
                self.set('newCategoryName', '');
                self.set('selectedCategory', category);
            }
        },

        selectCategory: function (category) {
            this.set('selectedCategory', category);
        },

        deleteCategoryRecursive: function(category) {
            //console.log("deleteCategoryRecursive: " + category.get('name'));
            var self = this;

            var parent = category.get('parent');
            if (!Ember.isEmpty(parent)) {
                parent.get('subElements').removeObject(category);
                if (parent.get('subElements').get('length') === 0)
                  parent.set('isExpanded', false);
            }

            // properties aus Category löschen
            var props = category.get('properties');
            var pr = props.get('firstObject');
            while(pr !== undefined)
            {
                // console.log("Schleife: " +  pr);
                props.removeObject(pr);
                pr.deleteRecord();
                pr.save();
                pr = props.get('firstObject');
            }

            // Sub Categorien löschen
            var subCategories = category.get('subElements');
            var subCategory = subCategories.get('firstObject');
            while(subCategory !== undefined)
            {
                //console.log("Schleife: " +  subCategory.get('name'));
                subCategories.removeObject(subCategory);
                self.send("deleteCategoryRecursive", subCategory);
                subCategory = subCategories.get('firstObject');
            }

            // Kategory löschen
            category.deleteRecord();
            category.save();
        },

        deleteCategory: function(category) {
            var self = this;
            self.get('model').removeObject(category);
            self.send('deleteCategoryRecursive', category);
            self.set('selectedCategory', undefined);
            //self.get('model').save();
            self.transitionToRoute('categories.index');
        },

        deleteSelectedCategory: function() {
            if (this.get('selectedCategory') !== undefined) {
                this.send("deleteCategory", this.get('selectedCategory'));
            }
        },

        deleteAll: function() {
            console.log("deleteAll");
            var self = this;
            self.get('model').clear();
            //console.log("localStorage.length: " + localStorage.length);
            self.store.unloadAll('category');
            self.store.unloadAll('property');
            localStorage.clear();
            self.set('selectedCategory', undefined);
            self.transitionToRoute('categories.index');
        },

        readXML: function() {

            var xml = "<?xml version='1.0' encoding='UTF-8'?>" +
                    "<properties>" +
                    "<category name='imageworkflow'>" +
                    "<property name='fotodialogonimport' value='true'/>" +
                    "<property name='iptcdialogonimport' value='true'/>" +
                    "</category>" +
                    "<category name='textcategories'>" +
                    "<category name='headline'>" +
                    "<property name='index' value='2'/>" +
                    "<property name='default' value='false'/>" +
                    "<property name='de' value='Hauptüberschrift'/>" +
                    "<property name='en' value='Headline'/>" +
                    "</category>" +
                    "<category name='overline'>" +
                    "<property name='index' value='0'/>" +
                    "<property name='de' value='Leiste'/>" +
                    "<property name='de_DE' value='Leiste (D)'/>" +
                    "<property name='en' value='Overline'/>" +
                    "</category>" +
                    "</category>" +
                    "<category name='linetypes'>" +
                    "<property name='Gepunktete Linie' value='dash=0 2,cap=rwround,dashFillIndex=1'/>" +
                    "<property name='Gestrichelte Linie' value='dash=2 1,cap=but,dashFillIndex=1'/>" +
                    "<property name='Einfache Linie' value=''/>" +
                    "</category>" +
                    "<category name='Pictogram'>" +
                    "<property name='DefaultBuz' value='74b8dce2ea078b3c'/>" +
                    "<category name='for_article_nn'>" +
                    "<property name='57b5be' value='613735ee095049a4'/>" +
                    "</category>" +
                    "</category>" +
                    "<category name='languages'>" +
                    "<property name='3' value='fr_FR'/>" +
                    "<property name='1' value='en_US'/>" +
                    "<property name='0' value='de_DE'/>" +
                    "</category>" +
                    "<category name='preferences'>" +
                    "<property name='separationofspotcolors' value='true'/>" +
                    "<property name='maxpathsegmentstopgroup' value='5000'/>" +
                    "<property name='maxpathsegments' value='3000'/>" +
                    "<property name='openpartpageinlayoutmode' value='false'/>" +
                    "<property name='pixelateenabled' value='true'/>" +
                    "<property name='rotateviewport' value='true'/>" +
                    "<property name='iptccaptionforbuz' value='true'/>" +
                    "<property name='openpageinlayoutmode' value='false'/>" +
                    "<property name='usephotoimportdialog' value='true'/>" +
                    "<property name='scribblefontsize' value='15'/>" +
                    "</category>" +
                    "<category name='photometafields'>" +
                    "<property name='multilinefields' value='IPTC_25,IPTC_105,IPTC_120,IPTC_231,EXIF_IFD0_33432,EXIF_IFD0_270,XMP_Rights,XMP_Description,XMP_Subject'/>" +
                    "<category name='equalfields'>" +
                    "<property name='1' value='IPTC_80,EXIF_IFD0_315,XMP_Creator'/>" +
                    "<property name='2' value='IPTC_120,EXIF_IFD0_270,XMP_Description'/>" +
                    "<property name='3' value='IPTC_5,XMP_Title'/>" +
                    "<property name='4' value='IPTC_25,XMP_Subject'/>" +
                    "<property name='5' value='IPTC_116,EXIF_IFD0_33432,XMP_Rights'/>" +
                    "</category>" +
                    "<category name='extractedfields'>" +
                    "<category name='IPTC_110'>" +
                    "<property name='displayname' value='Bildrechte (eigener Displaystring)'/>" +
                    "<property name='required' value='false'/>" +
                    "<property name='sortorder' value='1'/>" +
                    "<property name='default' value='Norbert Reher'/>" +
                    "</category>" +
                    "<category name='IPTC_101'>" +
                    "<property name='sortorder' value='2'/>" +
                    "</category>" +
                    "<category name='IPTC_95'>" +
                    "<property name='sortorder' value='3'/>" +
                    "</category>" +
                    "<category name='EXIF_IFD0_270'>" +
                    "<property name='required' value='true'/>" +
                    "<property name='sortorder' value='4'/>" +
                    "</category>" +
                    "<category name='EXIF_IFD0_305'>" +
                    "<property name='required' value='true'/>" +
                    "<property name='sortorder' value='5'/>" +
                    "</category>" +
                    "<category name='EXIF_IFD0_33432'>" +
                    "<property name='required' value='true'/>" +
                    "<property name='sortorder' value='6'/>" +
                    "</category>" +
                    "<category name='XMP_Subject'>" +
                    "<property name='required' value='false'/>" +
                    "<property name='sortorder' value='7'/>" +
                    "</category>" +
                    "<category name='XMP_Description'>" +
                    "<property name='required' value='false'/>" +
                    "<property name='sortorder' value='8'/>" +
                    "</category>" +
                    "</category>" +
                    "</category>" +
                    "</properties>";

            this.readXMLString(xml)
        },

        downloadXML: function() {

            console.log("downloadXML Beginn");
            var self = this;

            //this.send("deleteAll");
            self.get('model').clear();

            console.log("localStorage.length: " + localStorage.length);

            for (var i = 0; i < localStorage.length; i++) {
                console.log("localStorage.key(i): " + localStorage.key(i));
            }

            self.store.unloadAll('category');
            self.store.unloadAll('property');
            localStorage.clear();

            console.log("vor ajax-call");
            $.ajax({
                type: "GET",
                //url: "http://sunrise.red-web.lan:8080/server/dv/rest/8100/ApplicationConfigurationService/downloadSystemConfiguration?user='nreher'&password='nreher'",
                //url: "http://sunrise.red-web.lan:8080/server/dv/rest/8100/ApplicationConfigurationService/downloadSystemConfigurationUnCompressed?user=%27nreher%27&password=%27nreher%27",
                url: "http://redweb4.local:8080/server/dv/rest/8104/ApplicationConfigurationService/downloadSystemConfigurationUnCompressed?user=%27nreher%27&password=%27nreher%27",

                //dataType: "xml",
                xhrFields: {
                    // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
                    // This can be used to set the 'withCredentials' property.
                    // Set the value to 'true' if you'd like to pass cookies to the server.
                    // If this is enabled, your server must respond with the header
                    // 'Access-Control-Allow-Credentials: true'.
                    withCredentials: false
                },

                success: function(data){
                    //console.log("success xml: " + data);

                    var xml = String.fromCharCode.apply(String, data);
                    console.log(xml);

                    var xmlDoc = $.parseXML( xml );
                    var $xml = $( xmlDoc );


                    var readCategory = function(cat, parent, mainCategory) {
                        // console.log($(this).attr("name"));
                        var category = self.store.createRecord('category', {
                            name: $(cat).attr("name"),
                            display: $(cat).attr("name"),
                            description: '',
                            parent: parent,
                            isExpanded: false
                        });
                        category.save();

                        $(cat).children('property').each(function () {
                            //console.log("name= " + $(this).attr("name") + " value= " + $(this).attr("value"));
                            var property = self.store.createRecord("property", {
                                name: $(this).attr("name"),
                                value: $(this).attr("value"),
                                description: '',
                                parent: category
                            });
                            property.save();
                            category.get('properties').pushObject(property);
                            category.save();
                        });
                        $(cat).children('category').each(function () {
                            readCategory(this, category, false);
                        });
                        if (mainCategory === true) {
                            self.pushObject(category);
                        }
                    };

                    $($xml).find('properties').children('category').each(function() {
                        readCategory(this, null, true);
                    });

                    console.log("readCategories Ende");
                    self.set('selectedCategory', undefined);
                    self.transitionToRoute('categories.index');
                },
                error: function() {
                    console.log("An error occurred while processing XML file.");
                }
            });
            console.log("nach ajax-call");

            //self.get('model').save();

//       self.store.save(); geht nicht

        },

        writeXML: function() {
            //console.log('writeXML called');

            var self = this;
            var xmlString = '';
            var header =  "<?xml version='1.0' encoding='UTF-8'?><properties>";
            xmlString += header;

            var writeCategory = function(category) {
                xmlString += "<category name=\"1_" + category.get('name') + "\">";
                if (!Ember.isEmpty(category.get('subElements'))) {
                    category.get('subElements').forEach(function(subCategory) {
                        writeCategory(subCategory);
                    })
                }
                if (!Ember.isEmpty(category.get('properties'))) {
                    category.get('properties').forEach(function(property) {
                        xmlString += "<property name=\"" + property.get('name') + "\" value=\"" + property.get('value') + "\"/>";
                    })
                }
                //console.log(category.get('name'));
                xmlString += "</category>";
            };

            var model = self.get('model');
            model.forEach(function(category) {
                writeCategory(category);
            });

            xmlString += "</properties>";

            console.log(xmlString);

            // test
            self.readXMLString(xmlString);
        }
    },

    readXMLString: function(xml) {
        var self = this;
        self.get('model').clear();
        console.log("localStorage.length: " + localStorage.length);

        for (var i = 0; i < localStorage.length; i++) {
            console.log("localStorage.key(i): " + localStorage.key(i));
        }

        self.store.unloadAll('category');
        self.store.unloadAll('property');
        localStorage.clear();

        var xmlDoc = $.parseXML( xml ),
            $xml = $( xmlDoc ),
            $title = $xml.find( 'title' );

        var readCategory = function(cat, parent, mainCategory) {
            // console.log($(this).attr("name"));
            var category = self.store.createRecord('category', {
                name: $(cat).attr("name"),
                display: $(cat).attr("name"),
                description: '',
                parent: parent,
                isExpanded: false
            });
            category.save();

            $(cat).children('property').each(function () {
                //console.log("name= " + $(this).attr("name") + " value= " + $(this).attr("value"));
                var property = self.store.createRecord("property", {
                    name: $(this).attr("name"),
                    value: $(this).attr("value"),
                    description: '',
                    parent: category
                });
                property.save();
                category.get('properties').pushObject(property);
                category.save();
            });
            $(cat).children('category').each(function () {
                readCategory(this, category, false);
            });
            if (mainCategory === true) {
                self.pushObject(category);
            }
        };

        $($xml).find('properties').children('category').each(function() {
            readCategory(this, null, true);
        });

        console.log("readCategories Ende");
        self.set('selectedCategory', undefined);
        self.transitionToRoute('categories.index');

        //self.get('model').save();

//       self.store.save(); geht nicht

    },

});
