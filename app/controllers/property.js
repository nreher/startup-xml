import Ember from 'ember';

export default Ember.ObjectController.extend({

  isEditing: false,

  actions: {
    editProperty: function () {
      console.log('propertyController.editProperty');
      this.set('isEditing', true);
    },
    endEditProperty: function () {
      console.log('propertyController.endEditProperty');
      this.set('isEditing', false);
      this.get('model').save();
    }
  }
});
