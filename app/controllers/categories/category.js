import Ember from 'ember';

export default Ember.ObjectController.extend({

    actions: {

        addProperty: function() {
            this.set('isAdding', true);
        },

        createProperty: function() {
            this.set('isAdding', false);
            var self = this;

            var name = self.get('newName');
            var value = self.get('newValue');
            if (!Ember.isEmpty(name) && !Ember.isEmpty(value)) {
                var newProp = {name: name, value: value, display: '', description: ''};
                var property = this.store.createRecord('property', newProp);
                property.save();
                self.get('model.properties').pushObject(property);
                self.get('model').save();
            }
            self.set('newName', '');
            self.set('newValue', '');
        },

        deleteOneProperty: function(property) {
            var self = this;
            console.log("property: " + property);
            self.get('model.properties').removeObject(property);
            self.get('model').save();
            property.deleteRecord();
            property.save();
        },

        deleteProperties: function() {
            var self = this;
            var newProps = self.get('model.properties').filterBy('selected', true);
            for (var i = 0; i < newProps.length; i++)
            {
                var prop = newProps[i];
                console.log("prop[i]: " + prop);
                self.get('model.properties').removeObject(prop);
                self.get('model').save();
                prop.deleteRecord();
                prop.save();
            }
        }

    },

    hasNoSelectedProperties: function() {
        var props = this.get('properties');
        if (Ember.isEmpty(props) === false) {
            var check = props.isAny('selected', true);
            //console.log("CHECK : " + check);
            return !check;
        }
        return true;
    }.property('model.properties.@each.selected'),

    hasProperties: function() {
        var props = this.get('properties');
        return Ember.isEmpty(props) === false;
    }.property('model.properties'),

    displayTableHeader: function() {
        return Ember.isEmpty(this.get('properties')) === false || this.get('isAdding');
    }.property('model.properties', 'isAdding'),

    deleteAllProperties: function() {
        console.log("deleteAllProperties");
        /*
         var props = this.get('properties');
         if (!Ember.isEmpty(props)) {
         console.log("deleteAllProperties");
         }
         */
    }.property('model.groperties'),

    getIsExpanded: function() {
        return this.get('model.isExpanded');
    }.property('model.isExpanded'),

    getLevel: function() {
        //console.log('getLevel');
        var level = 0;
        var parent = this.get('parent');
        while(!Ember.isEmpty(parent)) {
            level++;
            parent = parent.get('parent');
        }
        return level;
    },

    hasSubCategories: function() {
        var subCategories = this.get('subElements');
        return Ember.isEmpty(subCategories) === false;
    }.property('model.subElements')

});
