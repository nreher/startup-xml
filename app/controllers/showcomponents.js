import Ember from 'ember';

export default Ember.Controller.extend({

    breadCrumb: 'Standardkomponenten',

    periods: Ember.A([
        { id:'any',    text: 'Beliebig'},
        { id:'hour',   text: 'Letzte Stunde'},
        { id:'day',    text: 'Letzte 24 Stunden'},
        { id:'week',   text: 'Letzte Woche'},
        { id:'month',  text: 'Letzten Monat'},
        { id:'year',   text: 'Letztes Jahr'},
        { id:'custom', text: 'Individuell'}
    ]),

    period:[],

    sources: Ember.A([
        { id:'1', text: 'Publishing Organizer'},
        { id:'2', text: 'Print Publisher'},
        { id:'3', text: 'Atlas Asset Manager'},
        { id:'4', text: 'Agentur'},
        { id:'5', text: 'System'},
        { id:'6', text: 'FindIT Topic'},
        { id:'7', text: 'ag_channel_ref'},
        { id:'8', text: 'Location mark analyzer'},
        { id:'9', text: 'agency/uniqueid'},
    ]),

    source:[],

    myselection:'',

    radioselection:'',

    colors: [{id:1,name:'Saphirschwarz metallic'},{id:2,name:"Valencia Orange metallic"},{id:3,name:"Estoril Blau"}]





});
