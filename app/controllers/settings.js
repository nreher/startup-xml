import Ember from 'ember';

export default Ember.Controller.extend({
    breadCrumb: 'Einstellungen',

    mode:'base',
    queryParams:['mode'],

    title: function () {
        switch (this.get('mode')) {
            case 'base': return 'Kontaktdaten';
            case 'password': return 'Kennwort ändern';
            case 'dashboard': return 'Dashboard';
            case 'presets': return 'Voreinstellungen';
        }
    }.property('mode'),

    contentpartial: function () {
        switch (this.get('mode')) {
            case 'base':
                return 'settings/-settings-content-base';
            case 'password':
                return 'settings/-settings-content-password';
            case 'dashboard':
                return 'settings/-settings-content-dashboard';
            case 'presets':
                return 'settings/-settings-content-presets';
        }

    }.property('mode'),
});
