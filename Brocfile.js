/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp({
    vendorFiles: {
        'handlebars.js': null
    }
});

// bootstrap
app.import('bower_components/bootstrap/dist/css/bootstrap.css');
app.import('bower_components/bootstrap/dist/css/bootstrap.css.map', { destDir: 'assets' });
app.import('bower_components/bootstrap/dist/js/bootstrap.js');

// select 2
app.import('bower_components/select2/select2.js');
app.import('bower_components/select2/select2.css');

// moment.js
app.import('bower_components/moment/moment.js');
app.import('bower_components/moment/locale/de.js');

module.exports = app.toTree();
