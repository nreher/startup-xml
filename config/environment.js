/* jshint node: true */

module.exports = function (environment) {
    var ENV = {
        modulePrefix: 'startup',
        environment: environment,
        baseURL: '/',
        locationType: 'auto',
        EmberENV: {
            FEATURES: {
            }
        },

        APP: {
            ATLAS_API_URL: "http://sunrise.red-web.lan:8080/atlas/api/v1",
            REDWEB_API_URL: "http://sunrise.red-web.lan:8080/server/dv/rest/8100/",
            DEFAULT_LANGUAGE: "en"
        },

        contentSecurityPolicy: {
            'default-src': "'none'",
            'script-src': "'self' http://sunrise.red-web.lan:8080/",
            'font-src': "'self' http://sunrise.red-web.lan:8080/",
            'connect-src': "'self' http://sunrise.red-web.lan:8080/ http://redweb4.local:8080",
            'img-src': "'self' http://sunrise.red-web.lan:8080/",
            'style-src': "'self' http://sunrise.red-web.lan:8080/",
            'media-src': "'self' http://sunrise.red-web.lan:8080/"
        }
    };

    if (environment === 'development') {
        ENV.APP.LOG_ACTIVE_GENERATION = true;
        ENV.APP.LOG_VIEW_LOOKUPS = true;
    }

    if (environment === 'test') {
        ENV.baseURL = '/';
        ENV.locationType = 'auto';
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;
        ENV.APP.rootElement = '#ember-testing';
    }

    if (environment === 'production') {
    }

    return ENV;
};
