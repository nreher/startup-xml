import {
  indent
} from '../../../helpers/indent';
import { module, test } from 'qunit';

module('IndentHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = indent(42);
  assert.ok(result);
});
