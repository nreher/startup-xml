import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('documentissue', {
  // Specify the other units that are required for this test.
  needs: ['model:user', 'model:edition', 'model:publication', 'model:sourcesystem',
    'model:sourcechannel']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
