import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('documentkeyword', {
  // Specify the other units that are required for this test.
  needs: ['model:user', 'model:keyword', 'model:sourcesystem', 'model:sourcechannel']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
