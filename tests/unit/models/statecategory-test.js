import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('statecategory', 'Statecategory', {
  // Specify the other units that are required for this test.
  needs: ['model:state', 'model:user', 'model:publication']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
