import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('documentcategory', {
  // Specify the other units that are required for this test.
  needs: ['model:user', 'model:category', 'model:sourcesystem', 'model:sourcechannel']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
