import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('edition', 'Edition', {
  // Specify the other units that are required for this test.
  needs: ['model:publication', 'model:user', 'model:department', 'model:state']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
