import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('folder', {
  // Specify the other units that are required for this test.
  needs: ['model:user', 'model:usergroup', 'model:relation']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
