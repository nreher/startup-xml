import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('videometa', {
  // Specify the other units that are required for this test.
  needs: ['model:user', 'model:documentfunction', 'model:priority', 'model:lock',
    'model:sourcechannel', 'model:sourcesystem', 'model:varianttype', 'model:documentcategory',
    'model:documentissue', 'model:documentkeyword', 'model:documentlink', 'model:documentlocation',
    'model:documentperson', 'model:documentreference', 'model:department', 'model:edition',
    'model:publication', 'model:state', 'model:relation', 'model:category', 'model:keyword',
    'model:person', 'model:statecategory']
});

test('it exists', function(assert) {
    var model = this.subject();
    assert.ok(!!model);
});
